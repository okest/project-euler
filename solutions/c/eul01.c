/*  Walter O'Keefe
 *  Created: 2014-11-24
 *
 *  Project Euler, Problem 01:
 *  Find the sum of all the multiples of 3 or 5 below 1000.
 *
 *  Code answer: 233168 [Correct]
 *  >> You are the 411036th person to have solved this problem.
 *
 *  TODO: 
 *     1. Implement command-line passing for test_factors and max_tests.
 *        Mostly just need to confirm how to expand argv into an array.
 */

/* -- Header -- */

#include <stdio.h>
#include "euler.h"

int eul01 (int n, int *test_factors);

/* -- Solution -- */

int main (void)
{
  int i, answer = 0, max_tests = 1000;
  int test_factors[] = { 3, 5 };

  for (i = 0; i < max_tests; i++)
    answer += eul01 (i, test_factors);
  
  /* Output */
  printf ("%d\n", answer);
  return 0;
}

/* Return nth number below max_tests, if it has a number in test_factors
 * as a factor. Else 0. */
int eul01 (int n, int *test_factors)
{  
  return (has_factor_in_array (n, test_factors) ? n : 0);
}
