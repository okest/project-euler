/* Walter O'Keefe (github: wtok)
 * euler.h: My functions for Project Euler solutions. */

/* -- Declarations -- */

#include <math.h>

/* Return number of elements in a given array */
#define array_numel(array) (sizeof (array) / sizeof *(array))

int array_sum (int *array);
int has_factor_in_array (int n, int *test_factors);
int is_prime (int n);
int fib (int n);

/* -- Definitions -- */

/* Find the sum of an array */
int array_sum (int *array)
{
  int i, sum = 0, len = array_numel (array);
  
  for (i = 0; i <= len; i++)
    sum += array[i];

  return sum;
}

/* Return true if n has a number in test_factors as a factor. */
int has_factor_in_array (int n, int *test_factors)
{
  int f, num_test_factors = array_numel (test_factors);

  for (f = 0; f < num_test_factors; f++)
    if (n % test_factors[f] == 0)
      return 1;

  return 0;
}

/* Determine if n is prime by simple trial division */
int is_prime (int n)
{
  if (n < 3)
    return 0;
  
  int d;
  double s = sqrt (n);
  
  for (d = 2; d < s; d++)
    if (n % d == 0) // if no remainder, number isn't prime
      return 0;
  
  return 1;
}

/* Return nth fib number */
int fib (int n)
{
  // Handle strange first two cases
  if (n < 2)
    return n;
  if (n == 2)
    return 1;
  
  struct fibs
  {
    int older, old, new;
  } f = { .older = 0, .old = 1, .new = 1 };

  int i;
  
  // start at 2 to account for above cases
  for (i = 2; i < n; i++)
    {
      f.older = f.old;
      f.old = f.new;
      f.new = f.old + f.older;
    }

  return f.new;
}
