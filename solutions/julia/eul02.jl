# Walter O'Keefe 2015-01-10
# Project Euler, problem 02
#   Each new term in the Fibonacci sequence is generated by adding the previous
#   two terms.By starting with 1 and 2, the first 10 terms will be:
#   1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
#   By considering the terms in the Fibonacci sequence whose values do not
#   exceed four million, find the sum of the even-valued terms.

# General algorithm:
#  Store the 3 relevant fib #'s, write a fibonacci loop.
#  Store even fibs. After loop, find sum.

function eul02(nMax::Int)
    fib = [3 2 1] # [new old oldest]

    evenFibs = zeros(1000); # store our answers
    evenFibs[1] = 2; # set this to be lazy
    evenFibsCount = 2; # increment this when we store another even fib.
                       # Start at 2 since we already set evenFibs[1]
    while fib[1] < nMax
        if ( fib[1] % 2 == 0 )
            evenFibs[evenFibsCount] = fib[1]
            evenFibsCount += 1;
        end
        # fib stuff
        fib[3] = fib[2]
        fib[2] = fib[1]
        fib[1] = fib[2] + fib[3]
    end
    return sum(evenFibs)
end
