# Walter O'Keefe
# Created 2015-01-10
# Project Euler, problem 03
#   The prime factors of 13195 are 5, 7, 13 and 29.
#   What is the largest prime factor of the number 600851475143?

eul03(n::Int) = maximum(collect(keys(factor(n)))) # rekt
