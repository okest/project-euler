# Walter O'Keefe
# Created 2015-01-12
# Project Euler, problem 05
#   2520 is the smallest number that can be divided by each of the numbers from
#   1 to 10 without any remainder. What is the smallest positive number that is
#   evenly divisible by all of the numbers from 1 to 20?

function eul05(maxn)
    ans = 1;
    while true # plenty of time
        for i = 1:maxn
            if ans % i != 0
                ans += 1
                break
            end
            i == maxn && return ans
        end
    end
end
