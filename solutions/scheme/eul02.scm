;;  Walter O'Keefe
;;  Created 2015-05-10
;;  Project Euler, problem 2:
;;  By considering the terms in the Fibonacci sequence whose values do 
;;  not exceed four million, find the sum of the even-valued terms.

;; recursively (inefficiently) compute the nth fib number
(define (fibrec n)
  (if (< n 2) n
      (+ (fibrec (- n 1)) (fibrec (- n 2)))))

;; todo: complete solution using fibrec

;; old horrifying solution:
(define (eul02a n-maximum)
  (let ((fib-old 1)
        (fib-cur 2)
        (fib-new 3)
        ;; Initialise fib-sum at 2, as we assume first two steps have
        ;; already happened. I'm only interested in even fibs, so only
        ;; store fib-sum = 2 as opposed to 3.
        (fib-sum 2))
    (do () 
        ;; quite probably not the right loop function to use, but
        ;; it guarantees that fib-sum is returned
        ((> fib-sum n-maximum) fib-sum)
      (set! fib-old fib-cur)
      (set! fib-cur fib-new)
      (set! fib-new (+ fib-old fib-cur))
      (if (even? fib-new)
          (set! fib-sum (+ fib-sum fib-new))))))


;; Input driver
(begin (display "Enter max fib sum: ")
       (display (eul02 (read))))
