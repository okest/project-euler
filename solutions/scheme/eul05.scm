;; wtok 2015-09-26
;; find the smallest positive number that is evenly divisible by all the
;; integers from 1 to 20.

(define (eul05 maxdenom)
  (let modloop ((testn 1) (denom 1))
    (cond ((> denom maxdenom) testn)
          ((= 0 (modulo testn denom)) (modloop testn (+ denom 1)))
          (else (modloop (+ testn 1) 1)))))


