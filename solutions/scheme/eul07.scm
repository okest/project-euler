;; wtok 2015-09-27
;; find the 10,001st prime number

(define (++ n)
  (+ n 1))

(define (prime? n)
  (if (even? n) #f
      (let loop ((div 3) (maxdiv (ceiling (sqrt n))))        
        (cond ((> div maxdiv) #t)
              ((= 0 (modulo n div)) #f)
              (else (loop (+ div 2) maxdiv))))))

(define (nth-prime n)
  (let loop ((prime-count 0)
             (testn 0))
    (if (prime? testn)
        (if (= prime-count n) testn
            (loop (++ prime-count) (++ testn)))        
        (loop prime-count (++ testn)))))

