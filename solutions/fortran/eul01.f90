! Walter O'Keefe
! Created 2015-07-05
! Project Euler, problem 01
! Find the sum of all multiples of 3 or 5 below 1000

PROGRAM eul01
  IMPLICIT NONE

  ! not doing num_factors or nmax by user input yet
  INTEGER, PARAMETER :: num_factors = 2, nmax = 1000
  INTEGER :: i = 0, f = 0, n = 0, answer = 0 ! should probably reuse iterators
  INTEGER :: test_factors(num_factors)
    
  WRITE(*,*) 'Enter factors, one per line:'
  DO i = 1,num_factors
     READ(*,*) test_factors(i)
  END DO
  
  ! actual solution
  DO n = 1,(nmax - 1)
     DO f = 1,num_factors
        IF(MOD(n, test_factors(f)) == 0) THEN
           answer = answer + n
           EXIT
        END IF
     END DO
  END DO

  ! output
  WRITE(*,*) answer
  
END PROGRAM eul01
