import std.stdio;
import euler;

int main ()
{
  int f, total, max = 4000000;

  for (int i; f < max; ++i)
    {
      f = fib (i);
      (f % 2 == 0) && (total += f);
    }

  writeln (total);
  return 0;
}

