/* This is good enough, right? */
import std.stdio;
import euler;

int main ()
{
  int answer;
  int[] test_factors = [ 3, 5 ];

  foreach (i; 0 .. 1000)
    answer += eul01 (i, test_factors);
  
  writeln(answer);
  return 0;
}

/* Return n if n has a number in test_factors as a factor, else 0. */
int eul01 (int n, int[] test_factors)
{
  return (has_factor_in_array (n, test_factors) ? n : 0);
}

