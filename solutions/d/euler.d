module euler;

/* Return true if n has a number in test_factors as a factor. */
bool has_factor_in_array (int n, int[] test_factors)
{
  foreach (f; test_factors)
    if (n % f == 0)
      return true;
  
  return false;
}

/* Inefficiently calculate and return nth fibonacci number */
int fib (int n)
{
  if (n < 2)
    return n;
  if (n == 2)
    return 1;

  struct fibs
  {
    int older, old, current;
  }

  auto f = fibs (0, 1, 1);

  foreach (int i; 2 .. n)
    {
      f.older = f.old;
      f.old = f.current;
      f.current = f.old + f.older;
    }

  return f.current;
}

