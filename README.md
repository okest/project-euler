# project-euler

My solutions to Project Euler problems in whatever languages take my
fancy. I'm using this to teach myself git and to learn the basics of
the languages used.

I'll iterate on the code here if I think I can make it better.

## C

I'm writing C with the GNU C Manual as a reference. I'm generally
avoiding GCC extensions; I'd like to have a good knowledge of C99
first.

## D

I've currently translated my C solutions to D quite directly. I
imagine I won't notice any significant difference between C and D
until I try more complicated problems.

## Scheme

I'm writing (fairly poor) R7RS Scheme and building it using Chicken
(R5RS) with my fingers crossed.

I'm learning Scheme through The Little Schemer and the R7RS standard.

## Julia

Julia is the first language I stuck out any Project Euler problems
with, so most of my other solutions are based on the Julia ones. The
comments are generally much less informative because I was learning as
I went, rather than beforehand.

